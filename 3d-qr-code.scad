/* [QR Code] */
qr_code_file = "qrcode.svg";
qr_code_thickness = 0.3; // [0:0.1:50]
qr_code_color = "black";
// Make sure this is greater 0 if it doesn't render
qr_code_smooth_radius = 1; // [0.001:0.01:10]
// Makes the QR
qr_code_smooth_inwards = true;
qr_code_margin = 2; // [0:0.1:50]

/* [Base] */
base_thickness = 0.3; // [0:0.1:50]
base_edge_radius = 1; // [0:0.01:50]
base_size = 50;       // [10:0.1:300]
base_color = "white";

qr_code_size = base_size - 2 * qr_code_margin > 0
                 ? base_size - 2 * qr_code_margin
                 : base_size;

/* [Precision] */
epsilon = 0.1;
$fs = $preview ? 1 : 0.5;
$fa = $preview ? 1 : 0.5;

module rounded_cube(size, radius = 0, center = false)
{
  translate([ radius, radius, epsilon / 2 ]) minkowski()
  {
    cube([ size[0] - 2 * radius, size[1] - 2 * radius, size[2] - epsilon ],
         center = center);
    cylinder(r = radius, h = epsilon, center = true);
  }
}

if (base_thickness > 0) {
  color(base_color) rounded_cube([ base_size, base_size, base_thickness ],
                                 radius = base_edge_radius);
}

translate([
  (base_size - qr_code_size) / 2,
  (base_size - qr_code_size) / 2,
  base_thickness -
  epsilon
]) color(qr_code_color) linear_extrude(qr_code_thickness + epsilon)
  offset((qr_code_smooth_inwards ? 1 : -1) * qr_code_smooth_radius)
    offset((qr_code_smooth_inwards ? -1 : 1) * qr_code_smooth_radius)
      resize([ qr_code_size, qr_code_size ]) import(qr_code_file);
