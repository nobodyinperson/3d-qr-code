![3D Printed QR Code](https://gitlab.com/nobodyinperson/3d-qr-code/-/raw/master/3d-qr-code.jpg)

# 3D Printable QR Code

This is an OpenSCAD design to 3D print a QR code.

## Usage

Generate an SVG QR code, for example with [`qrencode`](https://fukuchi.org/works/qrencode/):

```bash
qrencode -t SVG -o qrcode.svg -m 0 --background=00000000 "OpenSCAD"
```

>>>
Note that `qrencode` creates a solid background **even if using full transparency**. I created a [Merge Request](https://github.com/fukuchi/libqrencode/pull/184) to drop the background. Make sure to use a `qrencode` version including that Merge Request or manually delete the background, e.g. with Inkscape, otherwise, OpenSCAD will only see the background, not the QR Code.
>>>

Open the `.scad`-file and specify the path to your QR code file (or just leave it if you already called it `qrcode.svg`). You should see a 3D version of your QR Code. If rendering (F6) doesn't show the QR code anymore, try increasing `qr_code_smooth_radius`.

## 🖨 Printing

Recommended print settings that worked for me:

- ⬜ base in **white** (other colors makes the code difficult to scan)
- ⬛ QR code matrix in **black**
- 📏 0.15mm layer height
- 📏 0.4mm nozzle and don't exaggerate the extrusion width
- 2⃣ at least 2 layers for base and qr code matrix **each**
- 📏 QR matrix height set to something very low (e.g. 0.3mm) as more height introduces shadows which makes scanning more difficult
- maybe rotate the QR code by 45° so the solid infill is aligned nicely with the lines
